﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace TimeTrees.Desctop
{
    public class ConnectionCreation : Tool
    {
        Canvas CnvBack;
                
        public Point IsMouseDouwLocation;
        private int Relation;
        Relative FirstRelative;
        Relative SecondRelative;
        public bool IsMouseDown = false;
        private double StartX;
        private double StartY;
        private double CurX;
        private double CurY;
        Point StartDownLocation;
        private Polyline NewLine;
        PointCollection MyPointCollection;
        Rectangle SoursRect;
        public Rectangle ClickedRectangle;
        public ConnectionCreation(Canvas CnvBack, int Relation)
        {
            this.Relation = Relation;
            this.CnvBack = CnvBack;
            DoLine = true;
            DoNewConnection();
        }
        public void DoNewConnection()
        {
            CnvBack.MouseDown += CnvBack_LineMouseDown;
            CnvBack.MouseMove += DrawLineMouseMove;
            CnvBack.MouseUp += CnvBack_LineMouseUp;
        }
        private void CnvBack_LineMouseDown(object sender, MouseButtonEventArgs e)
        {
            IsMouseDown = true;
            if (e.OriginalSource is Rectangle)
            {
                FirstRelative = FindRelative((Rectangle)e.OriginalSource, null);
                ClickedRectangle = (Rectangle)e.OriginalSource;
                SoursRect = (Rectangle)e.OriginalSource;
                StartX = e.GetPosition(CnvBack).X;
                StartY = e.GetPosition(CnvBack).Y;
                CurX = e.GetPosition(CnvBack).X;
                CurY = e.GetPosition(CnvBack).Y;
                StartDownLocation = new Point(StartX, StartY);
            }
            else
            {
                CnvBack.MouseDown -= CnvBack_LineMouseDown;
                CnvBack.MouseMove -= DrawLineMouseMove;
                CnvBack.MouseUp -= CnvBack_LineMouseUp;
            }
        }
        private void DrawLineMouseMove(object sender, MouseEventArgs e)
        {
            if (IsMouseDown == false)
            {
                return;
            }

            CurX = e.GetPosition(CnvBack).X;
            CurY = e.GetPosition(CnvBack).Y;

            if (NewLine == null)
            {
                NewLine = new Polyline
                {
                    Stroke = Brushes.Green,
                    StrokeThickness = 2
                };

                Canvas.SetZIndex(NewLine, 1);
                CnvBack.Children.Add(NewLine);
            }

            MyPointCollection = new PointCollection();
            MyPointCollection.Add(StartDownLocation);
            Point CurLocation = new Point(CurX, CurY);
            MyPointCollection.Add(CurLocation);
            NewLine.Points = MyPointCollection;
            DoHoverAffect(NewLine);
            CnvBack.InvalidateVisual();
        }
        private void DrawLineBetweenRects(double mouseX, double mouseY)
        {
            MyPointCollection = new PointCollection();
            MyPointCollection.Add(StartDownLocation);
            Point CurLocation = new Point(mouseX, mouseY);
            MyPointCollection.Add(CurLocation);
            NewLine.Points = MyPointCollection;
            ClickedRectangle = null;
        }
        private void CnvBack_LineMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (DoLine == true)
            {
                if (e.OriginalSource is Rectangle)
                {
                    DropHoverAffect(NewLine);
                    SecondRelative = FindRelative((Rectangle)e.OriginalSource, null);
                    if (Relation == Spouse)
                    {
                        FirstRelative.person.Spouse = SecondRelative.person;
                        SecondRelative.person.Spouse = FirstRelative.person;
                    }
                    else
                    {
                        FirstRelative.person.Children.Add(SecondRelative.person);
                    }
                    double mouseX = e.GetPosition(CnvBack).X;
                    double mouseY = e.GetPosition(CnvBack).Y;
                    DrawLineBetweenRects(mouseX, mouseY);
                    ConnectionsInfo.Add(new RectConnection
                    {
                        StartRect = SoursRect,
                        FinishRect = (Rectangle)e.OriginalSource,
                        Line = NewLine,
                        Relation = Relation
                    });
                    UpdateConnection(ConnectionsInfo[ConnectionsInfo.Count - 1]);
                    DropHoverAffect(RightConnection.Line);
                }
                else
                {
                    NewLine.Points = null;
                }
            }
            NewLine = null;
            IsMouseDown = false;
            DoLine = false;
            CnvBack.MouseDown -= CnvBack_LineMouseDown;
            CnvBack.MouseMove -= DrawLineMouseMove;
            CnvBack.MouseUp -= CnvBack_LineMouseUp;           
        }
    }
}
