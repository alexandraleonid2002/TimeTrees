﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace TimeTrees.Desctop
{
    class RelationType: Tool
    {
        private Border DataBorder;
        private StackPanel DataPanel;
        public Canvas CnvBack;
        ConnectionCreation connectionCreation;

        public RelationType(Canvas CnvBack)
        {
            this.CnvBack = CnvBack;
            FindType();
        }
        public void FindType()
        {
            DataBorder = new Border()
            {
                CornerRadius = new CornerRadius(5),
                Margin = new Thickness(5, 110, 0, 0),
                Width = 120,
                BorderBrush = Brushes.Gray,
                Background = Brushes.AliceBlue,
                BorderThickness = new Thickness(2, 2, 2, 2),
                Padding = new Thickness(8),

            };
            DataPanel = new StackPanel()
            {
                Name = "DataPanel",
                Orientation = Orientation.Horizontal
            };
            Button BtnSpouse = new Button()
            {
                Content = "Spouse",
                Width = 50
            };
            DataPanel.Children.Add(BtnSpouse);
            Button BtnChild = new Button()
            {
                Content = "Child",
                Width = 50
            };
            DataPanel.Children.Add(BtnChild);
            DataBorder.Child = DataPanel;
            CnvBack.Children.Add(DataBorder);
            Canvas.SetZIndex(DataBorder, 10);
            BtnSpouse.Click += BtnSpouse_Click;
            BtnChild.Click += BtnChild_Click;            
        }

        private void BtnChild_Click(object sender, RoutedEventArgs e)
        {
            connectionCreation = new ConnectionCreation(CnvBack, Child);
            CnvBack.Children.Remove(DataPanel);
        }

        private void BtnSpouse_Click(object sender, RoutedEventArgs e)
        {
            connectionCreation = new ConnectionCreation(CnvBack, Spouse);
            CnvBack.Children.Remove(DataPanel);
        }
    }
}
