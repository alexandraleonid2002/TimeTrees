﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Shapes;

namespace TimeTrees.Desctop
{
    class RectLocation : Tool
    {
        bool HoverLine = false;
        Canvas CnvBack;
        StatusBarHelper statusBarHelper;
        bool IsPressed = false;
        Rectangle ClickedRectangle;
        public RectLocation(Canvas CnvBack, StatusBarHelper statusBarHelper)
        {
            this.statusBarHelper = statusBarHelper;
            this.CnvBack = CnvBack;
            RectMove();
        }
        public void RectMove()
        {
            CnvBack.MouseLeftButtonDown += CnvBack_MouseLeftButtonDown;
            CnvBack.MouseLeftButtonUp += CnvBack_MouseLeftButtonUp;
            CnvBack.MouseMove += CnvBack_MouseMove;
        }
        private void CnvBack_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.OriginalSource is Rectangle && DoLine == false)
            {
                IsPressed = true;
                ClickedRectangle = (Rectangle)e.OriginalSource;
                DoHoverAffect(ClickedRectangle);
                ClickedRectangle.CaptureMouse();
            }
        }
        private void CnvBack_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (e.OriginalSource is Rectangle && DoLine == false)
            {
                if (HoverLine == true)
                {
                    HoverLine = false;
                }
                DropHoverAffect(ClickedRectangle);
                ClickedRectangle.ReleaseMouseCapture();
                ClickedRectangle = null;
                IsPressed = false;
            }
        }
        private void CnvBack_MouseMove(object sender, MouseEventArgs e)
        {
            Point Cursor = Mouse.GetPosition(CnvBack);
            statusBarHelper.UpDateCoordinates(Cursor);
            if (e.OriginalSource is Rectangle && DoLine == false)
            {
                if (IsPressed == true)
                {
                    double mouseX = e.GetPosition(CnvBack).X;
                    double mouseY = e.GetPosition(CnvBack).Y;
                    Canvas.SetLeft(ClickedRectangle, mouseX);
                    Canvas.SetTop(ClickedRectangle, mouseY);
                    Relative ClickedRelative = null;
                    ClickedRelative = FindRelative((Rectangle)e.OriginalSource, ClickedRelative);
                    if (ClickedRelative != null)
                    {
                        Canvas.SetLeft(ClickedRelative.DataText, mouseX);
                        Canvas.SetTop(ClickedRelative.DataText, mouseY);
                        if (People != null)
                        {
                            People[IndexPerson].CoordinatesX = mouseX;
                            People[IndexPerson].CoordinatesY = mouseY;
                        }
                    }
                    foreach (RectConnection connection in ConnectionsInfo)
                    {
                        if (connection.StartRect == (Rectangle)e.OriginalSource || connection.FinishRect == (Rectangle)e.OriginalSource)
                        {
                            HoverLine = true;         
                            UpdateConnection(connection);
                        }
                    }
                }
            }
        }
    }
}
