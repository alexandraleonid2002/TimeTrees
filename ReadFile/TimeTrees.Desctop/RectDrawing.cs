﻿using System.Text;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using TimeTrees.Core;

namespace TimeTrees.Desctop
{
    class RectDrawing : Tool
    {
        static StringBuilder DataTxt;
        public static (Relative, Rectangle) Execute(Canvas CnvBack, Relative NewRelative, Rectangle NewRectangle, Person person)
        {
            if (NewRectangle == null)
            {
                NewRectangle = new Rectangle()
                {
                    Width = 70,
                    Height = 70,
                    Stroke = Brushes.Gray,
                    Fill = Brushes.Bisque,
                    StrokeThickness = 3
                };

                CnvBack.Children.Add(NewRectangle);
                Canvas.SetZIndex(NewRectangle, 5);

                NewRelative = new Relative()
                {
                    rectangle = NewRectangle,
                    person = person
                };
                Relatives.Add(NewRelative);
                Canvas.SetZIndex(NewRelative.DataText, 6);
                CnvBack.Children.Add(NewRelative.DataText);
            }
            Canvas.SetTop(NewRectangle, person.CoordinatesY);
            Canvas.SetLeft(NewRectangle, person.CoordinatesX);
            DoHoverAffect(NewRectangle);
            EditPerson(NewRelative);
            Canvas.SetLeft(NewRelative.DataText, Canvas.GetLeft(NewRelative.rectangle));
            Canvas.SetTop(NewRelative.DataText, Canvas.GetTop(NewRelative.rectangle));

            return (NewRelative, NewRectangle);
        }
        private static void EditPerson(Relative relative)
        {
            Person person = relative.person;
            DataTxt = new StringBuilder();
            if (person.Name != null && person.Name != "")
            {
                DataTxt.Append(person.Name);
            }
            if (person.DateBirth != null && person.DateBirth.ToString("yyyy.MM.dd") != "0001.01.01")
            {
                DataTxt.Append("\n" + person.DateBirth.ToString("yyyy.MM.dd"));
            }
            if (person.DateDeath != null && person.DateDeath.ToString() != "")
            {
                DataTxt.Append("\n- " + person.DateDeath.Value.ToString("yyyy.MM.dd"));
            }
            relative.DataText.Content = DataTxt.ToString();
        }
    }
}

