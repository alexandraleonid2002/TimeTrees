﻿using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using TimeTrees.Core;

namespace TimeTrees.Desctop
{
    class TreeDrawing: Tool
    {
        public PointCollection MyPointCollection;
        Canvas CnvBack;
        public TreeDrawing(Canvas CnvBack)
        {
            this.CnvBack = CnvBack;
            DrawTree();
        }
        private void DrawTree()
        {
            if (People != null && People.Count > 0)
            {
                for (int i = 0; i < People.Count; i++)
                {
                    (Relative NewRelative, Rectangle NewRectangle) = RectDrawing.Execute(CnvBack, null, null, People[i]);
                    DropHoverAffect(NewRectangle);
                }
                foreach (Relative relative in Relatives)
                {
                    if (relative.person.Spouse != null)
                    {
                        DrawSpouseConnection(relative);
                    }
                    if (relative.person.Children.Count > 0)
                    {
                        DrawChildrenConnection(relative);
                    }
                }
            }            
        }
        public void DrawChildrenConnection(Relative relative)
        {
            foreach(Person child in relative.person.Children)
            {
                int childId = 1;
                foreach (Relative kid in Relatives)
                {
                    if (kid.person.Id == child.Id)
                    {
                        childId = kid.person.Id;
                    }
                }
                DrawNewLine(relative, childId, Child);
            }
        }
        public void DrawSpouseConnection(Relative relative)
        {
            int spouseId = 1;
            foreach (Relative spouse in Relatives)
            {
                if (spouse.person.Id == relative.person.Spouse.Id)
                {
                    spouseId = spouse.person.Id;
                }
            }
            DrawNewLine(relative, spouseId, Spouse);
        }
        public void DrawNewLine(Relative relative, int neededId, int relation)
        {
            ConnectionsInfo.Add(new RectConnection()
            {
                StartRect = relative.rectangle,
                FinishRect = Relatives[neededId - 1].rectangle,
                Line = new Polyline()
                {
                    Stroke = Brushes.Green,
                    StrokeThickness = 2
                },
                Relation = relation
            });
            Canvas.SetZIndex(ConnectionsInfo[ConnectionsInfo.Count - 1].Line, 1);
            CnvBack.Children.Add(ConnectionsInfo[ConnectionsInfo.Count - 1].Line);
            UpdateConnection(ConnectionsInfo[ConnectionsInfo.Count - 1]);
            DropHoverAffect(RightConnection.Line);
        }
    }
}
