﻿using System.Windows;
using TimeTrees.Core;
using System.Collections.Generic;

namespace TimeTrees.Desctop
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>   
    public partial class MainWindow : Window
    {
        StatusBarHelper statusBarHelper;
        RectCreation rectCreation;
        RelationType relationType;
        RectLocation rectLocation;
        DataAddition dataAddition;
        public MainWindow()
        {
            InitializeComponent();
            Tool.People = FileReader.ReadPeopleJsonFile(@"../../../../people.json");
            if (Tool.People == null)
            {
                Tool.People = new List<Person>();
            }
            TreeDrawing treeDrawing = new TreeDrawing(CnvBack);
            statusBarHelper = new StatusBarHelper(LblCoordinates);
            rectLocation = new RectLocation(CnvBack, statusBarHelper);
           
        }
        private void BtnRect_Click(object sender, RoutedEventArgs e)
        {
            rectCreation = new RectCreation(CnvBack);
        }       
        private void BtnConect_Click(object sender, RoutedEventArgs e)
        {
            relationType = new RelationType(CnvBack);
        }
        private void BtnData_Click(object sender, RoutedEventArgs e)
        {
            dataAddition = new DataAddition(CnvBack);
        }
        private void BtnSaveTree_Click(object sender, RoutedEventArgs e)
        {
            FileWriter.WritePeopleJsonFile(Tool.People);
        }
    }
}
