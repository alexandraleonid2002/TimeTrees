﻿using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Shapes;
using TimeTrees.Core;

namespace TimeTrees.Desctop
{
    public class RectCreation : Tool
    {
        public Canvas CnvBack;
        public Relative NewRelative;
        public Rectangle NewRectangle;
        Person NewPerson;        
        public RectCreation(Canvas CnvBack)
        {
            this.CnvBack = CnvBack;
            NewRect();
        }
        public void NewRect()
        {
            CnvBack.MouseMove += DrawRectangleMove;
            CnvBack.MouseDown += RectMouseDown;
        }
        private void RectMouseDown(object sender, MouseButtonEventArgs e)
        {
            People.Add(NewPerson);
            CnvBack.MouseMove -= DrawRectangleMove;
            CnvBack.MouseDown -= RectMouseDown;
            DropHoverAffect(NewRelative.rectangle);
            NewRectangle = null;
            NewPerson = null;
        }
        public void DrawRectangleMove(object sender, MouseEventArgs e)
        {
            double mouseX = e.GetPosition(CnvBack).X;
            double mouseY = e.GetPosition(CnvBack).Y;

            if (NewPerson == null)
            {
                NewPerson = new Person() { Id = People.Count + 1 };
                NewPerson.CoordinatesX = mouseX;
                NewPerson.CoordinatesY = mouseY;                
            }
            NewPerson.CoordinatesX = mouseX;
            NewPerson.CoordinatesY = mouseY;
   
            (NewRelative, NewRectangle) = RectDrawing.Execute(CnvBack ,NewRelative , NewRectangle, NewPerson);
        }
    }
}
