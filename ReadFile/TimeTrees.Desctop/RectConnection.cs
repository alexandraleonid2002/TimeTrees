﻿using System.Windows.Shapes;

namespace TimeTrees.Desctop
{ 
    public class RectConnection
    {
        public int Relation;
        public Rectangle StartRect;
        public Rectangle FinishRect;
        public Polyline Line;
    }
}
