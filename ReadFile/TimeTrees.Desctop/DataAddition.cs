﻿using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using TimeTrees.Core;

namespace TimeTrees.Desctop
{
    class DataAddition: Tool
    {
        Canvas CnvBack;
        private StringBuilder DataText;
        private TextBox NameBox;
        private TextBox BirthBox;
        private TextBox DeathBox;
        private Border DataBorder;
        private StackPanel DataPanel;
        private Button BtnSave;
        private Rectangle ChosenRect;
        private Relative ChosenRel;
        public DataAddition(Canvas CnvBack)
        {
            this.CnvBack = CnvBack;
            EditData = true;
            NewPers();
        }
        public void NewPers()
        {       
            CnvBack.MouseLeftButtonDown += CnvBack_MouseLeftButtonDown;
            CnvBack.MouseLeftButtonUp += CnvBack_MouseLeftButtonUp;
        }
        private void DoDataBoxes()
        {
            NameBox = new TextBox()
            {
                Name = "NameTxt"
            };
            BirthBox = new TextBox()
            {
                Name = "BirthTxt"
            };
            DeathBox = new TextBox()
            {
                Name = "DeathTxt"
            };
        }
        private void CnvBack_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.OriginalSource is Rectangle)
            {
                ChosenRect = (Rectangle)e.OriginalSource;
                ChosenRel = FindRelative(ChosenRect, ChosenRel);
                DataBorder = new Border()
                {
                    CornerRadius = new CornerRadius(5),
                    Margin = new Thickness(600, 100, 0, 0),
                    Width = 175,
                    BorderBrush = Brushes.Gray,
                    Background = Brushes.AliceBlue,
                    BorderThickness = new Thickness(2, 2, 2, 2),
                    Padding = new Thickness(8),
                    
                };
                DataPanel = new StackPanel()
                {
                    Name = "DataPanel",
                };
                Label NameData = new Label()
                {
                    Content = "_Name:"
                };
                DataPanel.Children.Add(NameData);
                NameBox = new TextBox()
                {
                    Name = "Name",
                };
                DataPanel.Children.Add(NameBox);
                Label BirthData = new Label()
                {
                    Content = "_Birth:"
                };
                DataPanel.Children.Add(BirthData);
                BirthBox = new TextBox()
                {
                    Name = "Birth",
                };
                DataPanel.Children.Add(BirthBox);
                Label DeathData = new Label()
                {
                    Content = "_Death:"                   
                };
                DataPanel.Children.Add(DeathData);
                DeathBox = new TextBox()
                {
                    Name = "Death",
                };
                DataPanel.Children.Add(DeathBox);
                BtnSave = new Button(){
                    Content = "Save"
                };
                ShowPersonData();
                BtnSave.Click += BtnSave_Click;
                DataPanel.Children.Add(BtnSave);
                DataBorder.Child = DataPanel;
                CnvBack.Children.Add(DataBorder);
                Canvas.SetZIndex(DataBorder, 10);
            };
        }
        private void ShowPersonData()
        {
            Person chosenPers = ChosenRel.person;
            if (chosenPers.Name != null && chosenPers.Name != "")
            {
                NameBox.Text = chosenPers.Name;
            }
            if (chosenPers.DateBirth.ToString() != "" && chosenPers.DateBirth.ToString("yyyy.MM.dd") != "0001.01.01")
            {
                BirthBox.Text = chosenPers.DateBirth.ToString("yyyy.MM.dd");
            }
            if (chosenPers.DateDeath != null && chosenPers.DateDeath.ToString() != "")
            {
                DeathBox.Text = chosenPers.DateDeath.Value.ToString("yyyy.MM.dd");
            }
        }
        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            EditPerson(ChosenRel);
            CnvBack.Children.Remove(DataBorder);
        }
        private void EditPerson(Relative relative)
        {
            DataText = new StringBuilder();
            if (NameBox.Text != null && NameBox.Text != "")
            {
                DataText.Append(NameBox.Text);
                relative.person.Name = NameBox.Text;
            }
            if (BirthBox.Text != null && BirthBox.Text != "")
            {
                relative.person.DateBirth = FileHelper.ParseDate(BirthBox.Text);
                DataText.Append("\n" + BirthBox.Text);
            }
            if (DeathBox.Text != null && DeathBox.Text != "")
            {
                relative.person.DateDeath = FileHelper.ParseDate(DeathBox.Text);
                DataText.Append("\n- " + DeathBox.Text);
            }
            ShowData(relative, DataText.ToString());
            EditPeople(ChosenRel);
        }
        private void ShowData(Relative relative, string dataText)
        {
            relative.DataText.Content = dataText;
        }     
        private void CnvBack_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            CnvBack.MouseLeftButtonDown -= CnvBack_MouseLeftButtonDown;
            CnvBack.MouseLeftButtonUp -= CnvBack_MouseLeftButtonUp;
        }
    }
 }
