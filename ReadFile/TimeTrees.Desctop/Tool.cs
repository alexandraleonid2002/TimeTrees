﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Shapes;
using TimeTrees.Core;

namespace TimeTrees.Desctop
{
    public abstract class Tool
    {
        public static List<RectConnection> ConnectionsInfo = new List<RectConnection>();
        public static RectConnection RightConnection;
        public static List<Relative> Relatives = new List<Relative>();
        public static List<Person> People = new List<Person>();
        public static bool DoLine = false;
        public static bool EditData = false;
        public int IndexPerson;
        public static int Spouse = 0;
        public static int Child = 1;

        public static void DoHoverAffect(Shape shape)
        {
            if (shape != null)
            {
                shape.Effect = new DropShadowEffect
                {
                    Color = Colors.Orange,
                    BlurRadius = 50,
                    Direction = 0,
                    ShadowDepth = 0
                };
            }
        }
        public void DropHoverAffect(Shape shape)
        {
            shape.Effect = null;
        }
        public Relative FindRelative(Rectangle chosenRect, Relative chosenRel)
        {
            foreach (Relative relative in Relatives)
            {
                if (chosenRect == relative.rectangle)
                {
                    chosenRel = relative;
                }
            }
            if (chosenRel != null && People != null)
            {
                for (int i = 0; i < People.Count; i++)
                {
                    if (chosenRel.person == People[i])
                    {
                        IndexPerson = i;
                    }
                }
            }
            return chosenRel;
        }
        public void EditPeople(Relative ChosenRel)
        {
            if (People != null)
            {
                People[IndexPerson] = ChosenRel.person;
            }
            else
            {
                People = new List<Person>() { ChosenRel.person };
            }
        }
        public void UpdateConnection(RectConnection connection)
        {
            Point startLocation;
            Point finishLocation;
            PointCollection MyPointCollection = new PointCollection();
            double startX = Canvas.GetLeft(connection.StartRect);
            double startY = Canvas.GetTop(connection.StartRect);
            double finishX = Canvas.GetLeft(connection.FinishRect);
            double finishY = Canvas.GetTop(connection.FinishRect);
            if (connection.Relation == Spouse)
            {
                if(startX < finishX)
                {
                    startLocation = new Point(startX + 70, startY + 35);
                    finishLocation = new Point(finishX, finishY + 35);
                }
                else
                {
                    startLocation = new Point(startX, startY + 35);
                    finishLocation = new Point(finishX + 70, finishY + 35);
                }
            }
            else
            {
                startLocation = new Point(startX + 35, startY + 70);
                finishLocation = new Point(finishX + 35, finishY);
            }
            MyPointCollection.Add(startLocation);
            MyPointCollection.Add(finishLocation);
            connection.Line.Points = MyPointCollection;
            RightConnection = connection;
        }
    }
}
