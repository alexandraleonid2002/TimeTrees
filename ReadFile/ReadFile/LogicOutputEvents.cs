﻿using System;
using System.Collections.Generic;
using TimeTrees.Core;

namespace TimeTrees.ConsoleGui
{
    class LogicOutputEvents
    {
        static public void Execute(List<TimeLineEvent> Events)
        {
            TimeLineEvent neededEvent = FindEvent(Events);
            EventData(neededEvent);
            ConsoleKeyInfo key = Console.ReadKey();
            Program.ReturnToMainMenu();
        }
        static void EventData(TimeLineEvent date)
        {
            Console.Clear();
            List<Person> parties = date.Participants;
            Console.WriteLine($"Дата: {date.DateEvent} \nОписание: {date.Discription}\nУчастники: ");
            int i = 3;
            foreach (Person party in parties)
            {
                Console.SetCursorPosition(12, i);
                Console.Write(party.Id + "\t" + party.Name);
                Console.SetCursorPosition(40, i);
                Console.WriteLine(party.DateBirth.ToString("yyyy-MM-dd") + "       " +
                         (party.DateDeath.HasValue ? party.DateDeath.Value.ToString("yyyy-MM-dd") : "жив"));
                i += 1;
            }
        }
        static TimeLineEvent FindEvent(List<TimeLineEvent> Events)
        {
            int? selectedIndex = 0;
            TimeLineEvent neededEvent = null;
            do
            {
                Console.Clear();
                WriteFoundedEvents(Events, selectedIndex);
                ConsoleKeyInfo ckey = Console.ReadKey();
                if (ckey.Key == ConsoleKey.DownArrow)
                {
                    if (selectedIndex == 0)
                    {
                        selectedIndex = 1;
                    }
                    else if (selectedIndex + 1 < Events.Count)
                    {
                        selectedIndex += 1;
                    }
                    else
                    {
                        selectedIndex = 0;

                    }
                }
                else if (ckey.Key == ConsoleKey.UpArrow)
                {
                    if (selectedIndex == 0)
                    {
                        selectedIndex = Events.Count - 1;
                    }
                    else
                    {
                        selectedIndex -= 1;
                    }
                }
                else if (Char.IsLetter(ckey.KeyChar) == true)
                {
                    selectedIndex = 0;
                }
                else if (ckey.Key == ConsoleKey.Enter)
                {
                    if (selectedIndex != null)
                    {
                        neededEvent = Events[selectedIndex.Value];
                        break;
                    }
                }
                else if (ckey.Key == ConsoleKey.Escape)
                {
                    break;
                }

            } while (true);

            return neededEvent;
        }
        static void WriteFoundedEvents(List<TimeLineEvent> Events, int? selectedIndex)
        {
            for (int i = 0; i < Events.Count; i++)
            {
                if (selectedIndex == i)
                {
                    Console.BackgroundColor = ConsoleColor.DarkRed;
                }
                Console.WriteLine(Events[i].DateEvent.ToShortDateString() + "\t" + Events[i].Discription);

                Console.BackgroundColor = ConsoleColor.Black;
            }
        }
    }
}
