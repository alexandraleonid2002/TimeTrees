﻿using System;
using System.Collections.Generic;
using TimeTrees.Core;

namespace TimeTrees.ConsoleGui
{
    class LogicDeltaMaxAndMin
    {
        static public void Execute(List<TimeLineEvent> Events)
        {
            WriteDiffMaxAndMin(Events);
        }
        static (DateTime, DateTime) MinAndMaxDate(List<TimeLineEvent> Events)
        {
            DateTime minDate = DateTime.MaxValue;
            DateTime maxDate = DateTime.MinValue;
            for (int i = 0; i < Events.Count; i++)
            {
                DateTime date = Events[i].DateEvent;
                if (date < minDate) minDate = date;
                if (date > maxDate) maxDate = date;
            }
            return (maxDate, minDate);
        }
        static (int, int, int) DifMinAndMaxDate(List<TimeLineEvent> Events)
        {
            (DateTime maxDate, DateTime minDate) = MinAndMaxDate(Events);
            int difYears = maxDate.Year - minDate.Year;
            int difMonths = maxDate.Month - minDate.Month;
            int difDays = maxDate.Day - minDate.Day;

            return (difYears, difMonths, difDays);
        }
        static void WriteDiffMaxAndMin(List<TimeLineEvent> Events)
        {
            (int difYears, int difMonths, int difDays) = DifMinAndMaxDate(Events);
            Console.WriteLine($"Между максимальной и минимальной датами прошло: {difYears} лет, {difMonths} месяцев, {difDays} дней");

        }

    }
}
