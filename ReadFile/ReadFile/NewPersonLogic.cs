﻿using System;
using System.Collections.Generic;
using TimeTrees.Core;

namespace TimeTrees.ConsoleGui
{
    class NewPersonLogic
    {
        static public void Execute(List<Person> People, string Exe)
        {
            Console.Clear();
            (string name, string dateBirth, string dateDeath, int? parent1Id, int? parent2Id) = GetPeopleData(People);
            (bool check, string mail) = CheckPeopleData(name, dateBirth, dateDeath);
            if (check == true)
            {
                List<Person> newPeople = AddNewPerson(People, name, dateBirth, dateDeath, parent1Id, parent2Id);
                FileWriter.NewPerson(newPeople, Exe, name, dateBirth, dateDeath, parent1Id, parent2Id);
                Console.Clear();
                Console.Write("Данные введены.");
            }
            else
            {
                Console.WriteLine(mail);
            }
            Program.ReturnToMainMenu();
        }
        static (bool, string) CheckPeopleData(string name, string dateBirth, string dateDeath)
        {
            if (name == "" || dateBirth == "")
            {
                return (false, "Неполный ввод данных");
            }
            else
            {
                if (dateDeath != "" && FileHelper.ParseDate(dateBirth) > FileHelper.ParseDate(dateDeath))
                {
                    return (false, "Дата рождения не может быть больше даты смерти");
                }
                else
                {
                    return (true, "Данные коректны");
                }
            }
        }
        static (string, string, string, int?, int?) GetPeopleData(List<Person> People)
        {
            Console.CursorVisible = true;
            Console.Write("Ведите имя человека: ");
            string name = Console.ReadLine();
            Console.Write("Ведите дату рождения человека: ");
            string dateBirth = Console.ReadLine();
            Console.Write("Ведите дату смерти человека, если имеется: ");
            string dateDeath = Console.ReadLine();

            (int? parent1Id, int? parent2Id) = ParentsExist(People);

            return (name, dateBirth, dateDeath, parent1Id, parent2Id);
        }
        static (int?, int?) ParentsExist(List<Person> People)
        {
            int? parent1Id = null;
            int? parent2Id = null;
            Console.Write("Нажмите Enter, если у человека неизвестны родители или кнопку F, чтобы найти родственников");
            ConsoleKeyInfo ckey = Console.ReadKey();
            if (ckey.Key == ConsoleKey.Enter)
            {
                return (null, null);
            }
            else
            {
                parent1Id = FindParent(People);
                Console.WriteLine("Хотите указать второго родителя? Если да, нажмите Enter, если нет - Escape.");
                ConsoleKeyInfo key = Console.ReadKey();
                if (key.Key == ConsoleKey.Enter)
                {
                    parent2Id = FindParent(People);

                }
                return (parent1Id, parent2Id);
            }
        }
        static int FindParent(List<Person> People)
        {
            Person parent = FindPersonLogic.Execute(People);
            Console.Clear();
            Console.WriteLine("Вы выбрали " + parent.Name + ".");

            return parent.Id;
        }
        static List<Person> AddNewPerson(List<Person> People, string name, string dateBirth, string dateDeath, int? parent1Id, int? parent2Id)
        {
            int newPersonId;
            if (People != null)
            {
                newPersonId = People.Count + 1;
                People.Add(new Person() { Id = newPersonId, Name = name, DateBirth = FileHelper.ParseDate(dateBirth) });
            }
            else
            {
                newPersonId = 1;
                People = new List<Person> { new Person { Id = newPersonId, Name = name, DateBirth = FileHelper.ParseDate(dateBirth) } };
            }
            if (dateDeath != "")
            {
                People[newPersonId - 1].DateDeath = FileHelper.ParseDate(dateDeath);
            }
            if (parent1Id != null)
            {
                People[newPersonId - 1].Relatives.Add(People[(int)parent1Id]);
            }
            if (parent2Id != null)
            {
                People[newPersonId - 1].Relatives.Add(People[(int)parent2Id]);
            }

            return People;
        }
    }
}

