﻿using System;
using System.Collections.Generic;
using TimeTrees.Core;

namespace TimeTrees.ConsoleGui
{
    class FindPersonLogic
    {
        static public Person Execute(List<Person> People)
        {
            List<Person> founded = new List<Person>();
            int? selectedIndex = null;
            string partName = string.Empty;
            Person neededPerson = null;
            do
            {
                Console.Clear();
                string enterName = "Начните вводить имя: " + partName;
                Console.WriteLine(enterName);
                Console.CursorVisible = true;

                if (string.IsNullOrEmpty(partName) == true)
                {
                    founded = People;
                }
                else
                {
                    founded = FilterPeople(partName, People);
                }
                WriteFoundedPeople(founded, selectedIndex);
                Console.SetCursorPosition(enterName.Length, 0);
                ConsoleKeyInfo ckey = Console.ReadKey();
                if (ckey.Key == ConsoleKey.DownArrow)
                {
                    if (selectedIndex == null)
                    {
                        selectedIndex = 0;
                    }
                    else if (selectedIndex + 1 < founded.Count)
                    {
                        selectedIndex += 1;
                    }
                    else
                    {
                        selectedIndex = 0;

                    }
                }
                else if (ckey.Key == ConsoleKey.UpArrow)
                {
                    if (selectedIndex == null || selectedIndex == 0)
                    {
                        selectedIndex = founded.Count - 1;
                    }
                    else
                    {
                        selectedIndex -= 1;
                    }
                }
                else if (Char.IsLetter(ckey.KeyChar) == true)
                {
                    partName += ckey.KeyChar;
                    selectedIndex = null;
                }

                else if (ckey.Key == ConsoleKey.Backspace)
                {
                    partName = partName.Remove(partName.Length - 1);
                }
                else if (ckey.Key == ConsoleKey.Enter)
                {
                    if (selectedIndex != null)
                    {
                        neededPerson = founded[selectedIndex.Value];
                        break;
                    }
                }
                else if (ckey.Key == ConsoleKey.Escape)
                {
                    break;
                }

            } while (true);

            return neededPerson;
        }
        static List<Person> FilterPeople(string partName, List<Person> people)
        {
            List<Person> filteredPeople = new List<Person>();
            foreach (Person person in people)
            {
                if (person.Name.Contains(partName) == true)
                {
                    filteredPeople.Add(person);
                }
            }
            return filteredPeople;
        }
        static void WriteFoundedPeople(List<Person> founded, int? selectedIndex)
        {
            for (int i = 0; i < founded.Count; i++)
            {
                if (selectedIndex == i)
                {
                    Console.BackgroundColor = ConsoleColor.DarkRed;
                }
                Console.Write(founded[i].Id + "\t" + founded[i].Name);
                Console.SetCursorPosition(45, i + 1);
                Console.WriteLine(founded[i].DateBirth.ToString("yyyy-MM-dd") + "       " +
                         (founded[i].DateDeath.HasValue ? founded[i].DateDeath.Value.ToString("yyyy-MM-dd") : "жив"));

                Console.BackgroundColor = ConsoleColor.Black;
            }
        }
    }
}
