﻿using System;
using System.Collections.Generic;
using TimeTrees.Core;

namespace TimeTrees.ConsoleGui
{
    class LogicEnterData
    {
        public static int EnterPerson = 1;
        public static int EnterEvent = 2;
        static public void Execute(List<TimeLineEvent> Events, List<Person> People, string Exe)
        {
            List<MenuItems> Items = new List<MenuItems>
            {
                new MenuItems { Id = EnterPerson, Task = "Ввести данные для человекa"},
                new MenuItems { Id = EnterEvent, Task = "Ввести данные для события"}
            };
            Console.CursorVisible = false;
            do
            {
                int selectedMenu = DrawMenuLogic.Execute(Items);
                if (selectedMenu == EnterPerson)
                {
                    Console.Clear();
                    NewPersonLogic.Execute(People, Exe);
                    Console.Read();
                }
                else if (selectedMenu == EnterEvent)
                {
                    Console.Clear();
                    NewTimeLineLogic.Execute(Events, People, Exe);
                    Console.Read();
                }
                else
                {
                    Console.WriteLine("");
                }
            } while (true);
        }
    }
}
