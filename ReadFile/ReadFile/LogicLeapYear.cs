﻿using System;
using System.Collections.Generic;
using TimeTrees.Core;

namespace TimeTrees.ConsoleGui
{
    class LogicLeapYear
    {
        static public void Execute(List<Person> People)
        {
            WriteNeededNames(People);
        }
        static void WriteNeededNames(List<Person> People)
        {
            Console.WriteLine("Родились в високосный год и не больше 20 лет: ");
            for (int i = 0; i < People.Count; i++)
            {
                (int age, DateTime dateBirth) = Age(People[i]);
                bool check = CheckAge(age, dateBirth);
                if (check == true)
                {
                    Console.WriteLine(People[i].Name);
                }
            }
        }
        static bool CheckAge(int age, DateTime dateBirth)
        {
            bool check = false;
            if ((age < 21) & (dateBirth.Year % 4 == 0))
            {
                check = true;
            }
            return check;
        }
        static (int, DateTime) Age(Person human)
        {
            DateTime Birth = human.DateBirth;
            DateTime date1;
            if (human.DateDeath == null)
            {
                date1 = DateTime.Today;
            }
            else
            {
                date1 = (DateTime)human.DateDeath;
            }
            int age = date1.Year - Birth.Year;

            return (age, Birth);
        }
    }
}
