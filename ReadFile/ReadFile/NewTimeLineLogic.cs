﻿using System;
using System.Collections.Generic;
using TimeTrees.Core;

namespace TimeTrees.ConsoleGui
{
    class NewTimeLineLogic
    {
        static public void Execute(List<TimeLineEvent> Events, List<Person> People, string Exe)
        {
            Console.Clear();
            (string date, string eventDiscription, List<Person> parties) = GetTimeLineData(People);
            if (date == "" || eventDiscription == "")
            {
                Console.WriteLine("Неполный ввод данных.");
            }
            else
            {
                List<TimeLineEvent> newEvents = AddNewTimeLine(date, eventDiscription, parties, Events);
                FileWriter.NewTimeLine(newEvents);
                Console.WriteLine("\n Данные введены.");
            }
            Program.ReturnToMainMenu();
        }
        static (string, string, List<Person>) GetTimeLineData(List<Person> People)
        {
            Console.CursorVisible = true;
            Console.Write("Введите дату, когда произошло событие: ");
            string date = Console.ReadLine();
            Console.Write("Введите описание события: ");
            string eventeDiscripton = Console.ReadLine();
            List<Person> parties = PartiesExist(People);

            return (date, eventeDiscripton, parties);
        }
        static List<Person> PartiesExist(List<Person> People)
        {
            List<Person> parties = new List<Person>();
            Console.WriteLine("Если хотите добавить участников, нажмите F, если нет - Enter");
            ConsoleKeyInfo keyInfo = Console.ReadKey();

            while (keyInfo.Key == ConsoleKey.F)
            {
                Person person = FindPersonLogic.Execute(People);
                parties.Add(person);
                Console.Clear();
                Console.WriteLine("Вы добавили " + person.Name + "\nХоите довить еще? Если да, нажмите F, если нет - Escape");
                keyInfo = Console.ReadKey();
            }
            return parties;
        }
        static List<TimeLineEvent> AddNewTimeLine(string data, string discription, List<Person> parties, List<TimeLineEvent> Events)
        {
            if (Events != null)
            {
                Events.Add(new TimeLineEvent() { DateEvent = FileHelper.ParseDate(data), Discription = discription, Participants = parties });
            }
            else
            {
                Events = new List<TimeLineEvent> { new TimeLineEvent { DateEvent = FileHelper.ParseDate(data), Discription = discription, Participants = parties } };
            }
            return Events;
        }
    }
}
