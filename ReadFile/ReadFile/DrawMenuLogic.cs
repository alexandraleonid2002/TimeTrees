﻿using System;
using System.Collections.Generic;
using TimeTrees.Core;

namespace TimeTrees.ConsoleGui
{
    class DrawMenuLogic
    {
        static int Index = 0;
        static public int Execute(List<MenuItems> items)
        {
            Console.Clear();
            for (int i = 0; i < items.Count; i++)
            {
                if (i == Index)
                {
                    Console.BackgroundColor = ConsoleColor.DarkGreen;
                    Console.ForegroundColor = ConsoleColor.White;

                    Console.WriteLine(items[i].Task);
                }
                else
                {
                    Console.WriteLine(items[i].Task);
                }
                Console.ResetColor();
            }
            ConsoleKeyInfo ckey = Console.ReadKey();
            if (ckey.Key == ConsoleKey.DownArrow)
            {
                if(Index == items.Count -1)
                {
                    Index = 0;
                }
                else
                {
                    Index += 1;
                }
            }
            else if (ckey.Key == ConsoleKey.UpArrow)
            {
                if (Index == 0)
                {
                    Index = items.Count - 1;
                }
                else
                {
                    Index -= 1;
                }
            }
            else if (ckey.Key == ConsoleKey.Enter)
            {
                int index = Index;
                Index = 0;
                return items[index].Id;
            }
            else
            {
                return 0;
            }
            Console.Clear();
            return 0;
        }
        
    }
}
