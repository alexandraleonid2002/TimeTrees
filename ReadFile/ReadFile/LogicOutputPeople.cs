﻿using System;
using System.Collections.Generic;
using TimeTrees.Core;

namespace TimeTrees.ConsoleGui
{
    class LogicOutputPeople
    {
        static public void Execute(List<Person> People)
        {
            Person neededPerson = FindPersonLogic.Execute(People);
            PersonData(neededPerson);
            ConsoleKeyInfo key = Console.ReadKey();
            Program.ReturnToMainMenu();
        }
        static void PersonData(Person person)
        {
            Console.Clear();
            Console.WriteLine($"Id: {person.Id} \nИмя: {person.Name}\nДата рождения: {person.DateBirth.ToShortDateString()}" +
                "\nДата смерти:" + (person.DateDeath.HasValue ? person.DateDeath.Value.ToShortDateString() : "жив") +
                "\nРодитель 1:" + (person.Relatives.Count != 0 ? person.Relatives[0].Name : "нет") +
                "\nРодитель 2:" + (person.Relatives.Count > 1 ? person.Relatives[1].Name : "нет"));

        }
    }
}
