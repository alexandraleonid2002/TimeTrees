﻿using System;
using System.Collections.Generic;
using TimeTrees.Core;

namespace TimeTrees.ConsoleGui
{
    class LogicReductPeople
    {
        static public void Execute(List<Person> People)
        {
            Person reductPerson = FindPersonLogic.Execute(People);
            Console.Clear();
            Console.WriteLine($"Вы выбрали {reductPerson.Name}.");
            Console.WriteLine("\nВыберите поле которое хотите изменить и нажмите на соответсвующуую клавишу:\nИмя - N\nПервый родитель - F1" +
                "\nВторой родитель - F2\nДата рождения - B\nДата смерти - D");
            ConsoleKeyInfo field = Console.ReadKey(true);
            People = ReductFiled(People, reductPerson.Id, field);
            FileWriter.Change(People);
            Console.WriteLine("Данные изменены");
            Console.Clear();
            Program.ReturnToMainMenu();
        }
        static List<Person> ReductFiled(List<Person> People, int id, ConsoleKeyInfo field)
        {
            switch (field.Key)
            {
                case (ConsoleKey.N):
                    Console.WriteLine($"Вы выбрали поле Имя: {People[id - 1].Name}");
                    Console.CursorVisible = true;
                    Console.WriteLine("Введите новое значение: ");
                    People[id - 1].Name = Console.ReadLine();
                    Console.CursorVisible = false;
                    break;
                case (ConsoleKey.F1):
                    ChangeParent(People, id, 0, "первого");
                    break;
                case (ConsoleKey.F2):
                    ChangeParent(People, id, 1, "второго");
                    break;
                case (ConsoleKey.B):
                    Console.WriteLine($"Вы выбрали поле даты рождения: {People[id - 1].DateBirth.ToString("yyyy-MM-dd")}");
                    Console.CursorVisible = true;
                    Console.Write("Введите новое значение: ");
                    People[id - 1].DateBirth = FileHelper.ParseDate(Console.ReadLine());
                    Console.CursorVisible = false;
                    break;
                case (ConsoleKey.D):
                    Console.WriteLine($"Вы выбрали поле даты смерти: {(People[id - 1].DateDeath.HasValue ? People[id - 1].DateDeath.Value.ToString("yyyy-MM-dd") : "жив")}");
                    Console.CursorVisible = true;
                    Console.WriteLine("Введите новое значение: ");
                    People[id - 1].DateDeath = FileHelper.ParseDate(Console.ReadLine());
                    Console.CursorVisible = false;
                    break;
                default:
                    throw new Exception("PRESSED KEY IS OUT OF RANGE");
            }
            return People;
        }
        static void ChangeParent(List<Person> People, int id, int relative, string description)
        {
            Console.WriteLine($"Вы выбрали поле {description} родителя: {People[id - 1].Relatives[relative]}");
            Console.CursorVisible = true;
            Console.WriteLine("Введите новое значение: ");
            People[id - 1].Relatives[relative] = People[int.Parse(Console.ReadLine())];
            Console.CursorVisible = false;
        }
    }
}
