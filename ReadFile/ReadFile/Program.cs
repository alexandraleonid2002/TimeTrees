﻿using System;
using System.Collections.Generic;
using TimeTrees.Core;

namespace TimeTrees.ConsoleGui
{
    public class MenuItems
    {
        public int Id;
        public string Task;
    }
    class Program
    {
        public const int DeltaMaxandMinId = 1;
        public const int LeapYearId = 2;
        public const int ReductId = 3;
        public const int AddDataId = 4;
        public const int OutputPeopleId = 5;
        public const int OutputEventsId = 6;
        public const int ExitId = 7;

        public static List<Person> People;
        public static List<TimeLineEvent> Events;
        public static string Exe;

        static void Main(string[] args)
        {
            Exe = FileHelper.GetExtention();
            People = FileReader.PeopleFile(Exe);
            Events = FileReader.TimeLineFile(People, Exe);

            Menu();
        }
        static public void Menu()
        {
            List<MenuItems> Menuitems = new List<MenuItems>
            {
                new MenuItems {Id = DeltaMaxandMinId, Task = "1.Найти дельту дат между событиями"},
                new MenuItems {Id = LeapYearId, Task = "2.Найти людей, родившихся в високосный год"},
                new MenuItems {Id = ReductId, Task = "3.Редактировать данные людей"},
                new MenuItems {Id = AddDataId, Task = "4.Добавить данные" },
                new MenuItems {Id = OutputPeopleId, Task = "5.Вывод списка людей"},
                new MenuItems {Id = OutputEventsId, Task = "6.Вывод списка событий" },
                new MenuItems {Id = ExitId, Task = "7.Выход"}
            };
            Console.CursorVisible = false;
            do
            {
                int selectedMenu = DrawMenuLogic.Execute(Menuitems);
                switch (selectedMenu)
                {
                    case DeltaMaxandMinId:
                        Console.Clear();
                        LogicDeltaMaxAndMin.Execute(Events);
                        Console.Read();
                        break;
                    case LeapYearId:
                        Console.Clear();
                        LogicLeapYear.Execute(People);
                        Console.Read();
                        break;
                    case ReductId:
                        Console.Clear();
                        LogicReductPeople.Execute(People);
                        Console.Read();
                        break;
                    case AddDataId:
                        LogicEnterData.Execute(Events, People, Exe);
                        break;
                    case OutputPeopleId:
                        Console.Clear();
                        LogicOutputPeople.Execute(People);
                        Console.Read();
                        break;
                    case OutputEventsId:
                        LogicOutputEvents.Execute(Events);
                        break;
                    case ExitId:
                        Exit();
                        break;
                }
            } while (true);
        }
        static public void ReturnToMainMenu()
        {
            Console.WriteLine("\nХотите продолжить?");
            ConsoleKeyInfo ckey = Console.ReadKey(true);
            if (ckey.Key == ConsoleKey.Enter)
            {
                Console.Clear();
                Program.Menu();
            }
        }
        static public void Exit()
        {
            Console.Clear();
            ReturnToMainMenu();
        }
    }
}
