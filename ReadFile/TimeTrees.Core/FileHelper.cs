﻿using System;

namespace TimeTrees.Core
{
    public class FileHelper
    {
        private const int IdIndex = 0;
        private const int NameIndex = 1;
        private const int parent1Index = 2;
        private const int parent2Index = 3;
        private const int BirthIndex = 4;
        private const int DeathIndex = 5;
        static public DateTime ParseDate(string date)
        {
            DateTime parsedDate;
            if (DateTime.TryParseExact(date, "yyyy.MM.dd", null, System.Globalization.DateTimeStyles.None, out parsedDate)) ;

            else if (DateTime.TryParseExact(date, "yyyy.MM", null, System.Globalization.DateTimeStyles.None, out parsedDate)) ;

            else if (DateTime.TryParseExact(date, "yyyy", null, System.Globalization.DateTimeStyles.None, out parsedDate)) ;

            else
                throw new Exception("WRONG FORMAT"); ;

            return parsedDate;
        }
        static public string GetExtention()
        {
            Console.Clear();
            string exe;
            Console.WriteLine("С каким файлом вы хотите работать? Для .csv нажмите V, для .json нажмите J");
            ConsoleKeyInfo key = Console.ReadKey();
            if (key.Key == ConsoleKey.V)
            {
                exe = ".csv";
            }
            else
            {
                exe = ".json";
            }
            return exe;
        }
    }
}
