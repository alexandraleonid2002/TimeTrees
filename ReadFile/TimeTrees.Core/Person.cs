﻿using System;
using System.Collections.Generic;

namespace TimeTrees.Core
{
    public class Person
    {
        public int Id;
        public string Name;
        public DateTime DateBirth;
        public DateTime? DateDeath;
      //  public List<Person> Relatives = new List<Person>();
        public double CoordinatesX;
        public double CoordinatesY;
        public Person Spouse;
        public List<Person> Children = new List<Person>();
    }
}
