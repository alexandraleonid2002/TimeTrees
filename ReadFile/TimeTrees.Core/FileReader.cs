﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace TimeTrees.Core
{
    public class FileReader
    {
        private const int IdIndex = 0;
        private const int NameIndex = 1;
        private const int parent1Index = 2;
        private const int parent2Index = 3;
        private const int BirthIndex = 4;
        private const int DeathIndex = 5;
        static public List<Person> PeopleFile(string exe)
        {
            string path = @"../../../../people";
            if (exe == ".csv")
            {
                return ReadPeopleCsvFile(path + exe);
            }
            else
            {
                return ReadPeopleJsonFile(path + exe);
            }
        }
        static public List<TimeLineEvent> TimeLineFile(List<Person> People, string exe)
        {
            string path = @"../../../../timeline";
            if (exe == ".csv")
            {
                return ReadTimeLineCsvFile(People, path + exe);
            }
            else
            {
                return ReadTimeLineJsonFile(path + exe);
            }
        }
        public static List<Person> ReadPeopleJsonFile(string path)
        {
            List<Person> people = new List<Person>();
            string file = File.ReadAllText(path);
            List<Person> People = JsonConvert.DeserializeObject<List<Person>>(file);
            //  var People = JsonConvert.SerializeObject(file, Formatting.Indented);
            /* File.WriteAllText(@"D:\myJson.json", json);
             for (int i = 0; i < file.Length; i++)
             {
                 people.Add(JsonConvert.DeserializeObject<Person>(file[i]));
             }*/
            return People;
        }
        static List<TimeLineEvent> ReadTimeLineJsonFile(string path)
        {
            string file = File.ReadAllText(path);
            return JsonConvert.DeserializeObject<List<TimeLineEvent>>(file);
        }
        static string[][] ReadFile(string path)
        {
            string[] data = File.ReadLines(path).Where(line => line != "").ToArray();
            string[][] splitData = new string[data.Length][];
            for (int i = 0; i < data.Length; i++)
            {
                string line = data[i];
                string[] parts = line.Split(";");
                splitData[i] = parts;
            }
            return splitData;
        }
        static List<Person> ReadPeopleCsvFile(string path)
        {
            string[][] data = ReadFile(path);
            Person[] people = new Person[data.Length];
            for (int i = 0; i < data.Length; i++)
            {
                string[] parts = data[i];
                Person human = new Person();
                human.Id = int.Parse(parts[IdIndex]);
                human.Name = parts[NameIndex];
                human.DateBirth = FileHelper.ParseDate(parts[BirthIndex]);

                if (parts.Length == 6 && parts[DeathIndex] != "")
                {
                    human.DateDeath = FileHelper.ParseDate(parts[DeathIndex]);
                }
                else
                {
                    human.DateDeath = null;
                }

                if (parts[parent1Index] != "")
                {
                   // human.Relatives.Add(people[int.Parse(parts[parent1Index])]);
                }
                if (parts[parent2Index] != "")
                {
                   // human.Relatives.Add(people[int.Parse(parts[parent2Index])]);
                }

                people[i] = human;
            }
            return people.ToList();
        }

        private const int TimeLineIndex = 0;
        private const int EventIndex = 1;
        private const int PartiesIndex = 2;
        static List<TimeLineEvent> ReadTimeLineCsvFile(List<Person> People, string path)
        {
            string[][] data = ReadFile(path);
            TimeLineEvent[] events = new TimeLineEvent[data.Length];
            for (int i = 0; i < data.Length; i++)
            {
                string[] parts = data[i];
                TimeLineEvent lineEvent = new TimeLineEvent();
                lineEvent.DateEvent = FileHelper.ParseDate(parts[TimeLineIndex]);
                lineEvent.Discription = parts[EventIndex];
                if (parts[PartiesIndex] != "")
                {
                    string[] parties = parts[PartiesIndex].Split(", ");
                    if (parties.Length > 1)
                    {
                        foreach (string party in parties)
                        {
                            lineEvent.Participants.Add(People[int.Parse(party)]);
                        }
                    }
                    else
                    {
                        lineEvent.Participants.Add(People[int.Parse(parts[PartiesIndex])]);
                    }
                }
                events[i] = lineEvent;
            }
            return events.ToList();
        }
    }
}
