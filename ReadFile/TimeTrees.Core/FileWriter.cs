﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace TimeTrees.Core
{
    public class FileWriter
    {
        public static void WritePeopleJsonFile(List<Person> People)
        {
            string path = @"../../../../people.json";
            var json = JsonConvert.SerializeObject(People, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
            File.WriteAllText(path, json);

            /*string json = JsonConvert.SerializeObject(joe, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });*/
            /* File.WriteAllText(path, string.Empty);
             for (int i = 0; i < People.Count; i++)
             {
                 using (StreamWriter sw = new StreamWriter(path, true, System.Text.Encoding.Default))
                 {
                     sw.WriteLine(JsonConvert.SerializeObject(People[i]));
                 }
             }*/
        }
        static void WriteTimeLineJsonFile(List<TimeLineEvent> Events)
        {
            string json = JsonConvert.SerializeObject(Events);
            File.WriteAllText(@"../../../../timeline.json", json);
        }
        static public void Change(List<Person> People)
        {
            string parent1 = "";
            string parent2 = "";
            string path = "../../../../people.csv";
            File.WriteAllText(path, String.Empty);
            using (StreamWriter sw = new StreamWriter(path, true, System.Text.Encoding.Default))
            {
               /* for (int i = 0; i < People.Count; i++)
                {
                    if (People[i].Relatives.Count > 0)
                    {
                        parent1 = People[i].Relatives[0].Id.ToString();
                        if (People[i].Relatives.Count > 1)
                        {
                            parent2 = parent1 = People[i].Relatives[1].Id.ToString();
                        }
                    }
                    sw.WriteLine(People[i].Id + ";" + People[i].Name + ";" + parent1 + ";" + parent2 +
                        ";" + People[i].DateBirth.ToString("yyyy-MM-dd") + ";" + (People[i].DateDeath.HasValue ? People[i].DateDeath.Value.ToString("yyyy-MM-dd") : ""));
                }*/
            }
            WritePeopleJsonFile(People);
        }
        static public void NewTimeLine(List<TimeLineEvent> newEvents)
        {
            string path = "../../../../timeLine.csv";
            StringBuilder parties = new StringBuilder();
            TimeLineEvent newEvent = newEvents[^1];
            for (int i = 0; i < newEvent.Participants.Count; i++)
            {
                parties.Append(newEvent.Participants[i].Id.ToString() + ", ");
            }
            parties.Remove(parties.Length - 2, 2);
            using (StreamWriter sw = new StreamWriter(path, true, System.Text.Encoding.Default))
            {
                sw.WriteLine(newEvent.DateEvent.Year + ";" + newEvent.Discription + ";" + parties);
            }
            WriteTimeLineJsonFile(newEvents);
        }
        static public void NewPerson(List<Person> people, string exe, string name, string dateBirth, string dateDeath, int? parent1Id, int? parent2Id)
        {
            string path = "../../../../people.csv";
            int Id = people[people.Count - 1].Id;
            string parent1 = "";
            if (parent1Id != null)
            {
                parent1 = Convert.ToString(parent1Id);
            }
            string parent2 = "";
            if (parent2Id != null)
            {
                parent2 = Convert.ToString(parent2Id);
            }
            using (StreamWriter sw = new StreamWriter(path, true, System.Text.Encoding.Default))
            {
                sw.WriteLine(Id + ";" + name + ";" + parent1 + ";" + parent2 + ";" + dateBirth + ";" + dateDeath);
            }
            WritePeopleJsonFile(people);
        }
    }
}
