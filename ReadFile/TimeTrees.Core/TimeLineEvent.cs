﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeTrees.Core
{
    public class TimeLineEvent
    {
        public DateTime DateEvent;
        public string Discription;
        public List<Person> Participants = new List<Person>();
    }
}
